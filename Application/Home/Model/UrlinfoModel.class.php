<?php

namespace Home\Model;

use Think\Model;

class UrlinfoModel extends Model {
    
    //检查长网址是否存在
    public function is_exist($url){
        $res = $this->where(array('long_url'=>$url))->getField('short_url');
        if($res){
            return $res;
        }else{
            return false;
        }
    }
    
    //检查短网址是否存在
    public function get_short($id){
        $res = $this->where(array('id'=>$id))->getField('long_url');
        if($res){
            return $res;
        }else{
            return false;
        }
    }
    
    //增加点击数
    public function add_hot($id){
        $this->where('id='.$id)->setInc('hot'); 
    }
    
}