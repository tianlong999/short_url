<?php

namespace Home\Controller;

use Think\Controller;

class AjaxController extends CommonController {

    
    
    //ajax处理短网址生成
    public function index() {
        if (IS_AJAX) {
            $oriurl = I('post.oriurl');
            if(empty($oriurl)) echo '0';
            $oriurl = convertHttp($oriurl);
            $res = $this->model->is_exist($oriurl);
            if($res){
                echo $res;
                die;
            }
            $data = array(
                'long_url' => $oriurl,
                'short_url' => '',
                'time' => time(),
                'ip' => get_client_ip(0)
            );
            
            $url = new \Url();
            if ($this->model->add($data)) {
                $id = $this->model->getLastInsID();
                $short_url = $url->trans($id);
                if ($this->model->save(
                                array('short_url' => $short_url,
                                    'id' => $id
                                )
                        )) {
                    echo $short_url;
                }
            }
        }
    }
    
    //用于测试
    public function t(){
        
    }
  
}
