<?php

namespace Home\Controller;

use Think\Controller;

class RedirectController extends CommonController {
    
    //URL重定向
    public function index() {
        $id = I('get.id');
        $url = new \Url();
        $tran_id = $url->back_trans($id);
        $res = $this->model->get_short($tran_id);
        
        if ($res) {
            $this->model->add_hot($tran_id);
            header('location:' .$res, 302);
        } else {
            $r_url = U('Index/index',array('f'=>'e','id'=>$id),'');
            redirect($r_url);
        }
    }

    

}
