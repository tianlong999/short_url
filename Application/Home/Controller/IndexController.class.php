<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends CommonController {
    //首页显示
    public function index(){
        $flag = I('get.f');
        $id = I('get.id');
        if( !empty($flag) && !empty($id) ){
               $id = get_root() .$id;
               $this->nofind = '短网址 '.$id.' 无法找到，请检查您是否输入错误或者重新生成!!';
           
        }
        $this->root = get_root();
        $this->display();
        
    }
}
