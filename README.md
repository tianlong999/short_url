=======
## 在前面
本短网址生成器程序采用Thinkphp 3.2.2 + mysql 开发。

## 使用方法：
主要要求PHP5.3+版本，apache开启rewrite模块<br/>
如果你环境满足条件，直接运行即可安装

## 界面预览：
![1](http://git.oschina.net/uploads/images/2014/1104/111617_e93c46b2_106233.jpeg)

## 联系方式
有问题请留言我的<a href="http://blog.clmao.com/?page_id=145" target="_blank">博客</a>